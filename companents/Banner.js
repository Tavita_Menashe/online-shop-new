export default h("div", { className: "banner" }, [
  h("h1", { className: "banner__title" }, ["Lorem ipsum"]),
  h("button", { className: "btn banner__btn" }, ["Check"])
]);